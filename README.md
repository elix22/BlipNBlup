# [:four_leaf_clover:](https://www.luckeyproductions.nl/) Blip 'n Blup: Skyward Adventures

Two fish, a pact with Sa'Tong and a quest for food, enlightenment and trust.

![Blip 'n Blup screenshot](Screenshots/Screenshot_Fri_Dec_30_07_12_03_2016.png)