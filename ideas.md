# Story

The sea isn't the place it used to be (pollution, mutants, little food) and when Blip 'n Blup narrowly avoid a sinking ship - which turns out to be loaded with shoes - they decide to walk out.  
All their lives they've had to endure trash from above and wish to find the highest place, where there is no above. And so they head for the sky.

- Portals (Teleporting doors)
- Pits (Make things disappear)
- Double jump, second is aimed by its timing during the first.
- Angry creatures on bubble escape (float cooldown)


## Worlds

* **Beach** | 1: Landing
  + Worms
  + Crabs
* **Jungle** | 2: Into the wild
  + Frogs
  + Humming birds
  + Saddleback caterpillar (platform)
* **Leafy forest** | 3:
  + Snails
  + Wasps
  + Spiders
  + Streams (with floating logs as platforms)
* **Prickly forest** | 4:
  + Ants
* **Snowy peaks** | 5:
  + Aerial lifts
* **Volcano**
  + Lava streams with floating platforms
* **Lost temple** | 6:
  + Gnomes
* **Spaceship** | 7: Liftoff
  - Spoon

### Bonus

Bonus levels consist of collecting trash. Cans can be kicked, glass shards need a bubble.

## Multiplayer Gamemodes

* **_Capture the bug_** (CTF)
  + Longhorn beetle
  + Snail
* Footbubble
* Collector

## Characters

### Sa'Tong Lucifuge-Sterneel



### Spoon
> "I am that I am not"

## Inspiration

- Bubble Bobble

- Golf
- Marble Madness

- Wall-E
